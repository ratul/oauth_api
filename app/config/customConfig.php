<?php

return [
	'names' =>[
		'siteName' => 'Template'
	],
	'roles' =>[
		'admin' => 'admin',
		'user' => 'user'
	],
	'permissions' =>[
		'read'  => 'Read'
	],
];