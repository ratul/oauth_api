<?php

Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});

Route::group(['prefix'=> 'api/v1','before' => 'oauth'], function()
{


	Route::get('me', function(){

		$resourceOwnerType = Authorizer::getResourceOwnerType();
		$resourceOwnerId = Authorizer::getResourceOwnerId();


		if ($resourceOwnerType === 'user')
		{
			$user = Auth::loginUsingId($resourceOwnerId);
			return Response::json($user);
		}
		return Response::make('god hates me', 404);

	});

});