<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AuthclientsTableSeeder extends Seeder {

	public function run()
	{
		$clients = [
			[
				'id' => 'Wz0A5QM0Q3QSPO8L',
				'name' => 'ratelancer_api',
				'secret' => 'qHjNcvo67zaRRFka406MBMItF5Rmqbnf',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			]
		];
		DB::table('oauth_clients')
			->insert($clients);
	}

}